function Search(querySelectorInput, querySelectorOutput) {
    
    var input, 
        output,
        value;

    function _updateOutput(event) { //private
        value = input.value;
        console.log(value);
        output.innerHTML = value;
    }

    function init() { //public
        input = document.querySelector(querySelectorInput);
        output = document.querySelector(querySelectorOutput);
        console.log('running init', input, output);
        input.addEventListener('keyup', _updateOutput);
    }

    return {
        init: init
    }
}

var searchKeyAction = new Search('.input', '.message');
searchKeyAction.init();